let gallery = document.querySelector('.gallery');

class Card {
    constructor(author, userName, email, title, body, id) {
        this.author = author;
        this.userName = userName;
        this.email = email;
        this.title = title;
        this.body = body;
        this.id = id;
    }
    createPost() {

        let post = document.createElement('div');
        post.classList.add('post');
        gallery.append(post);

        let info = document.createElement('div');
        info.classList.add('info');
        post.append(info);

        let name = document.createElement('p');
        name.classList.add('name');
        name.innerText = this.author;
        info.append(name);

        let userName = document.createElement('p');
        userName.classList.add('username');
        userName.innerText = this.userName;
        info.append(userName);

        let email = document.createElement('p');
        email.classList.add('email');
        email.innerText = this.email;
        info.append(email);

        let deleteBtn = document.createElement('button');
        deleteBtn.classList.add('delete-btn');
        deleteBtn.innerText = `x`;
        info.append(deleteBtn);

        deleteBtn.addEventListener('click', (e) => {
            e.preventDefault();
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                method: "DELETE",
            })
                .then(() => post.remove())

        })

        let postText = document.createElement('div');
        postText.classList.add('post-text');
        post.append(postText);

        let title = document.createElement('p');
        title.classList.add('title');
        title.innerText = this.title;
        postText.append(title);

        let text = document.createElement('text');
        text.classList.add('text');
        text.innerText = this.body;
        postText.append(text);
    }
}
function getUser(id) {
    const url = new URL(`https://ajax.test-danit.com/api/json/users/${id}`);
    return fetch(url);
}
function getPosts() {
    const url = new URL('https://ajax.test-danit.com/api/json/posts');
    return fetch(url);
}
getPosts()
    .then((response) => response.json())
    .then(async posts => {
        console.log(posts);
        // всі пости обробляються паралельно
        let cards = await Promise.all(posts.map(async post => {

            let card = getUser(post.userId)
                .then((response) => response.json())
                .then(user => new Card (user.name, user.username, user.email, post.title, post.body, post.id))

            return card;
        }))

        //всі пости обробляються послідовно
        // for (const post of posts) {
        //     let userId = post.userId;
        //
        //     let user = await getUser(userId)
        //         .then((response) => response.json());
        //
        //
        //     let card = new Card (user.name, user.username, user.email, post.title, post.body, post.id);
        //     cards.push(card);
        // }

        return cards;
    })
    .then(posts => {
        for (const post of posts) {
            post.createPost();
        }

    })








